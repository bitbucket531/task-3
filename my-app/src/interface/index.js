import {
  useEffect,
  useState,
  useReducer,
  useCallback,
  useMemo,
  useDebugValue,
} from "react";
import useCreateList from "./customHook";
import "./styles.css";



const InterfaceAPI = () => {
  const [passVal, setVal] = useState(null);
  const [count, setCount] = useState(0);
  const myValue = useCreateList();

  const handleClick = () => {
    const val = document.getElementById("nazwa").value;
    setCount(count + 1);
    myValue.addElement(count,val);
    setVal(myValue.state);
  };

  return (
    <div>
      <div className="col-12 flex__wrapper">
        <p className="col-2">ID</p>
        <p className="col-2">Nazwa</p>
        <p className="col-2">Wykonać do</p>
        <p className="col-2">Wykonane</p>
        <p className="col-2">Usuń</p>
      </div>
      {passVal &&
        passVal.map((el, index) => {
          return (
            <div key="{index}" className="col-12 flex__wrapper">
              <p className="col-2">{el.id}</p>
              <p className="col-2">{el.name}</p>
              <p className="col-2">{el.date}</p>
              <p className="col-2">
                <input type="checkbox" checked={el.done} />
              </p>
              <button className="col-2">
                x
              </button>
            </div>
          );
        })}
      <div className="flex__wrapper">
        <div className="col-2">Nazwa:</div>
        <input type="textbox" className="col-2" id="nazwa" />
        <button onClick={() => handleClick()} className="col-2">
          Dodaj
        </button>
      </div>
    </div>
  );
};

export default InterfaceAPI;
