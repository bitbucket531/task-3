import React, { useState, useReducer, useEffect } from "react";
import ListItems from "./listItems"


const useCreateList = () => {
  const tablica = ListItems();
  const initialState = tablica.array;

  const addElement = (count, val) => {
    dispath({ type: "addNew", count: count, val: val });
  };


  const removeElement = () => {};

  const reducer = (state, action) => {
    switch (action.type) {
      case "addNew":
        tablica.addElement(action.count,action.val)
        return state;
      case "remove":
        state.slice(action.index, action.index + 1);
        return state;
    }
  };


  const [state, dispath] = useReducer(reducer, initialState);

  return { state, addElement, removeElement };
};

export default useCreateList;
